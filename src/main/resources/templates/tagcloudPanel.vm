#* Copyright (c) 2014 Siemens AG - Corporate Technology, Bernd Lindner

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*#


<form action="$req.contextPath/labels/tagcloud.action" method="get">

	<div class="tagcloud-filter-container first">
		<label for="filter-by-space">$action.getText('com.siemens.confluence.tagcloud.filter.space')</label>
        #bodytag ("Component" "id=filter-by-space" "theme='custom'" "name='key'" "template='space-select.vm'")
            #param ("value" $action.space.key)
            #param ("multiple" false)
            #param ("tabindex" 222)
        #end
	</div>

	<div class="tagcloud-filter-container">
		<label for="filter-daysBack">$action.getText('com.siemens.confluence.tagcloud.filter.daysback')</label>
		<select id="filter-daysBack" tabindex="223" name="daysBack">
			<option #if ($daysBack == "-1") selected #end value="-1">$action.getText('com.siemens.confluence.tagcloud.filteroption.anydate')</option>
			<option #if ($daysBack == "7") selected #end value="7">$action.getText('com.siemens.confluence.tagcloud.filteroption.lastweek')</option>
			<option #if ($daysBack == "30") selected #end value="30">$action.getText('com.siemens.confluence.tagcloud.filteroption.lastmonth')</option>
			<option #if ($daysBack == "180") selected #end value="180">$action.getText('com.siemens.confluence.tagcloud.filteroption.lastsixmonth')</option>
			<option #if ($daysBack == "365") selected #end value="365">$action.getText('com.siemens.confluence.tagcloud.filteroption.lastyear')</option>
		</select>
		<div class="tagcloud-filter-comment">$action.getText('com.siemens.confluence.tagcloud.filtercomment.daysback')</div>
	</div>

	<div class="tagcloud-filter-container">
		<label for="filter-maxResults">$action.getText('com.siemens.confluence.tagcloud.filter.maxResults')</label>
		<select id="filter-maxResults" tabindex="224" name="maxResults">
			<option #if ($maxResults == "-1") selected #end value="-1">$action.getText('com.siemens.confluence.tagcloud.filteroption.alltags')</option>
			<option #if ($maxResults == "50") selected #end value="50">$action.getText('com.siemens.confluence.tagcloud.filteroption.top50')</option>
			<option #if ($maxResults == "100") selected #end value="100">$action.getText('com.siemens.confluence.tagcloud.filteroption.top100')</option>
			<option #if ($maxResults == "200") selected #end value="200">$action.getText('com.siemens.confluence.tagcloud.filteroption.top200')</option>
			<option #if ($maxResults == "500") selected #end value="500">$action.getText('com.siemens.confluence.tagcloud.filteroption.top500')</option>
		</select>
		<div class="tagcloud-filter-comment">$action.getText('com.siemens.confluence.tagcloud.filtercomment.maxResults')</div>
	</div>

	<div class="tagcloud-filter-container">
		<label for="filter-format">$action.getText('com.siemens.confluence.tagcloud.filter.format')</label>
		<select id="filter-format" tabindex="225" name="format">
			<option #if ($format == "default") selected #end value="default">$action.getText('com.siemens.confluence.tagcloud.filteroption.classicaltagcloud')</option>
			<option #if ($format == "cumulusFlash") selected #end value="cumulusFlash">$action.getText('com.siemens.confluence.tagcloud.filteroption.3dball')</option>
			<option #if ($format == "cumulusCanvas") selected #end value="cumulusCanvas">$action.getText('com.siemens.confluence.tagcloud.filteroption.3dballCanvas')</option>
			<option #if ($format == "wordcloud") selected #end value="wordcloud">$action.getText('com.siemens.confluence.tagcloud.filteroption.wordcloud')</option>
		</select>
	</div>

	<input type="submit" value="$action.getText('com.siemens.confluence.tagcloud.filterbutton.showtagcloud')">
</form>
