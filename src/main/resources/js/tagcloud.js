/**
 * Copyright (c) 2016 Siemens AG - Corporate Technology, Bernd Lindner
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


function renderTagcloudJson(macroOccurrence) {
	var tagcloudContainer = AJS.$('#tagcloud-' + macroOccurrence);
	if (tagcloudContainer.hasClass('wordcloud')) {
		if (!WordCloud.isSupported) {
			return;
		}
		var ctx = tagcloudContainer[0].getContext('2d');
		ctx.canvas.width  = tagcloudContainer.width();
		ctx.canvas.height = tagcloudContainer.height();
	}

	var macroParams = new AjaxParams(macroOccurrence);
	macroParams.add("maxResults");
	macroParams.add("startDate");
	macroParams.add("endDate");
	macroParams.add("space");
	macroParams.add("types");
	macroParams.add("user");
	macroParams.add("cotag");
	macroParams.add("displayCotag");
	macroParams.add("excludePrefix");
	macroParams.add("excludePostfix");
	macroParams.add("scale");
	macroParams.add("sort");

	AJS.$.ajax({
		type : "GET",
		url : AJS.Data.get("context-path") + "/labels/tagcloudjson.action",
		data : macroParams.data,
		success : function(data) {
			if (data != null) {
				renderTagcloud(tagcloudContainer, data, macroOccurrence);
			} else {
				renderNoTagsMessage(tagcloudContainer);
			}
		}
	});

}

function renderTagcloud(tagcloudContainer, data, macroOccurrence) {
	var list = [];
	AJS.$.each(data.labels, function(i, val) {
		var labelTitle = val.title;
		var labelDisplayTitle = labelTitle;
		var truncateLength = AJS.Meta.get("tagcloud-truncate-" + macroOccurrence);
		if (truncateLength != -1 && labelDisplayTitle.length > truncateLength) {
			labelDisplayTitle = labelDisplayTitle.substring(0, truncateLength) + String.fromCharCode(8230);
		}
		// Unary plus operator to convert strings to numbers first (otherwise we get string concatenation).
		var fontSizePx = val.rank * AJS.Meta.get("tagcloud-fontSizeStep-" + macroOccurrence) + +AJS.Meta.get("tagcloud-fontMinSize-" + macroOccurrence);
		// wordcloud2.js actually expects a 2-dimensional list in the form of [word, size].
		// We build and pass a 6-dimensional list in the form of [displaytitle, size, count, fulltitle, id, isSimpleUrlPath]
		// to transport further attributes required on hover or click.
		list.push([labelDisplayTitle, fontSizePx, val.count, labelTitle, val.id, val.isSimpleUrlPath]);
	});
	
	if (tagcloudContainer.hasClass('wordcloud')) {
		renderTagcloudWordcloud(tagcloudContainer, list, macroOccurrence);
	} else {
		renderTagcloudClassic(tagcloudContainer, list, macroOccurrence);
	}

}

function renderTagcloudClassic(tagcloudContainer, list, macroOccurrence) {
	tagcloudContainer.find('#tagcloud-loading').hide();
	AJS.$.each(list, function(i, item) {
		var labelTitle = item[3];
		var labelCount = item[2];
		var fontSizePx = item[1];
		var labelDisplayTitle = item[0];
		var titleAttrValue = labelTitle + " - " + labelCount + " page" + (labelCount > 1 ? "s" : "");
		
		var tagElem = AJS.$("<a/>", {
			'class': 'rank',
			'title': titleAttrValue,
			'style': 'font-size: ' + fontSizePx + 'px',
			'text': labelDisplayTitle,
			'href': 'javascript:void(0)'
			});
		tagElem.data("item", item);
		tagcloudContainer.append(tagElem);
	});
	
	AJS.$('#tagcloud-' + macroOccurrence + ' a').after(" "); // separate <a> nodes to allow word wrapping
	AJS.$('#tagcloud-' + macroOccurrence + ' a').click(function (event) {
		tagcloudClick(AJS.$(this).data("item"), null, event);
	});
}

function renderTagcloudWordcloud(tagcloudContainer, list, macroOccurrence) {
	var options = {};
	options.list = list;
	options.fontFamily = "Times, serif";
	options.rotateRatio = AJS.Meta.get("tagcloud-rotateRatio-" + macroOccurrence);
	options.shape = AJS.Meta.get("tagcloud-shape-" + macroOccurrence);
	if (options.shape == "cardioid") {
		// vertical correction of cardioid center, otherwise the lower half it cut
		options.origin = [tagcloudContainer[0].width/2, 50];
	}
	options.ellipticity = 1;
	options.click = tagcloudClick;
	options.hover = wordcloudHover;
	WordCloud(tagcloudContainer[0], options);
}

function renderNoTagsMessage(tagcloudContainer) {
	if (tagcloudContainer.hasClass('wordcloud')) {
		renderNoTagsMessageCanvas(tagcloudContainer);
	} else {
		renderNoTagsMessageHtml(tagcloudContainer);
	}
}

function renderNoTagsMessageHtml(tagcloudContainer) {
	tagcloudContainer.find('#tagcloud-loading').hide();
	tagcloudContainer.append("<span>" + AJS.I18n.getText("com.siemens.confluence.tagcloud.macro.noTags") + "</span>");
}

function renderNoTagsMessageCanvas(tagcloudContainer) {
	var ctx = tagcloudContainer[0].getContext('2d');
	ctx.font="12px Arial";
	ctx.fillStyle="#FF0000";
	ctx.textAlign="center";
	ctx.fillText(AJS.I18n.getText("com.siemens.confluence.tagcloud.macro.noTags"), tagcloudContainer[0].width/2, tagcloudContainer[0].height/2);
}

function tagcloudClick(item, dimension, event) {
	var labelTitle = item[3];
	var labelId = item[4];
	var isSimpleUrlPath = item[5];
	var clickUrl = AJS.Data.get("context-path");
	if (AJS.$(event.target).hasClass('wordcloud')) {
		// click event on <canvas> element
		var macroOccurrence = AJS.$(event.target).attr('id').substring("tagcloud-".length);
	} else {
		// click event on <a> element
		var macroOccurrence = AJS.$(event.target).parent().attr('id').substring("tagcloud-".length);
	}
	var singleTargetSpace = AJS.Data.get("tagcloud-singleTargetSpace-" + macroOccurrence);
	if (isSimpleUrlPath) {
		if (singleTargetSpace) {
			clickUrl += "/label/" + singleTargetSpace + "/" + labelTitle;
		} else {
			clickUrl += "/label/" + labelTitle;
		}
	} else {
		if (singleTargetSpace) {
			clickUrl += "/labels/viewlabel.action?key=" + singleTargetSpace + "&ids=";
		} else {
			clickUrl += "/labels/viewlabel.action?ids=";
		}
		clickUrl += labelId;
		// append cotags
		var cotagIdsList = AJS.Meta.get("tagcloud-cotagIds-" + macroOccurrence);
		if (typeof cotagIdsList != 'undefined' && cotagIdsList.length != 0) {
			AJS.$.each(cotagIdsList.split(","), function(index, cotagId) {
				if (cotagId != labelId) {
					clickUrl += "&ids=" + cotagId;
				}
			});
		}
	}
	location.href = clickUrl;
}

function wordcloudHover(item, dimension, event) {
	if (dimension) {
		AJS.$('html').css('cursor', 'pointer');
	} else {
		AJS.$('html').css('cursor', 'auto');
	}
}

function AjaxParams(macroOccurrence) {
	this.macroOccurrence = macroOccurrence;
	this.data = {};
	
	this.add = function(name) {
		var value = AJS.Meta.get("tagcloud-" + name + "-" + macroOccurrence);
		if (typeof value != 'undefined' && value.length != 0) {
			this.data[name] = value;
		}
	};
}
