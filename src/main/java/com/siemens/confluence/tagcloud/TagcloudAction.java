/**
 * Copyright (c) 2014 Siemens AG - Corporate Technology, Bernd Lindner
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.siemens.confluence.tagcloud;

import java.util.ArrayList;
import java.util.List;

import com.atlassian.confluence.search.SpacePickerHelper;
import com.atlassian.confluence.search.SpacePickerHelper.SpaceDTO;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.actions.AbstractSpaceAction;
import com.atlassian.confluence.spaces.actions.SpaceAware;
import com.atlassian.core.util.PairType;
import com.atlassian.plugin.PluginAccessor;

/**
 * Implements SpaceAware to build breadcrumb.
 */
public class TagcloudAction extends AbstractSpaceAction implements SpaceAware {

	private static final long serialVersionUID = 6129722850594007890L;

	private SpacePickerHelper spacePickerHelper;
	private SpaceManager spaceManager;
	private Space space;
	private String maxResults = "200";
	private String daysBack = "365";
	private String format = null;

	@Override
	public String execute() {
		return SUCCESS;
	}

	public String getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(String maxResults) {
		if (maxResults != null) {
			this.maxResults = maxResults;
		}
	}

	public String getDaysBack() {
		return daysBack;
	}

	public void setDaysBack(String daysBack) {
		if (daysBack != null) {
			this.daysBack = daysBack;
		}
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		if (format != null) {
			this.format = format;
		}
	}

	@Override
	public Space getSpace() {
		return space;
	}

	@Override
	public void setSpace(Space space) {
		this.space = space;
	}

	@Override
	public boolean isSpaceRequired() {
		return false;
	}

	@Override
	public boolean isViewPermissionRequired() {
		return true;
	}

	/**
	 * Find all global spaces that user has permission to view, but that aren't favourites
	 * 
	 * @return List
	 */
	public List<SpaceDTO> getAvailableGlobalSpaces() {
		return getSpacePickerHelper().getAvailableGlobalSpaces(getAuthenticatedUser());
	}

	public List<PairType> getAggregateOptions() {
		// not all aggregations are implemented in tagcloud
		// return getSpacePickerHelper().getAggregateOptions(this);
		List<PairType> aggregateOptions = new ArrayList<PairType>();
		aggregateOptions.add(new PairType("", getText("inspace.allspace")));
		return aggregateOptions;
	}

	public List<SpaceDTO> getFavouriteSpaces() {
		return getSpacePickerHelper().getFavouriteSpaces(getAuthenticatedUser());
	}

	private SpacePickerHelper getSpacePickerHelper() {
		if (spacePickerHelper == null) {
			// get a spacePickerHelper for the spaces drop down
			spacePickerHelper = new SpacePickerHelper(spaceManager, labelManager);
		}
		return spacePickerHelper;
	}

	public void setSpaceManager(SpaceManager spaceManager) {
		this.spaceManager = spaceManager;
	}

	public void setPluginAccessor(PluginAccessor pluginAccessor) {
		this.pluginAccessor = pluginAccessor;
	}
}
