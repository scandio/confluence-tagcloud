/**
 * Copyright (c) 2011 Siemens AG - Corporate Technology, Bernd Lindner
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.siemens.confluence.tagcloud;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.SortedSet;
import java.util.TreeSet;

import com.atlassian.confluence.labels.actions.RankedNameComparator;
import com.atlassian.confluence.labels.actions.RankedRankComparator;
import com.atlassian.confluence.labels.persistence.dao.RankedLabelSearchResult;

public class RankLabels {

	private static final int SORTING_NAME = 1;
	private static final int SORTING_RANK = 2;
	private static final int SORTING_CIRCULAR = 3;
	private static final int SORTING_SIMILARITY = 4;
	
	private static final int SCALING_LINEAR = 1;
	private static final int SCALING_LOGARITHMIC = 2;
	private static final int SCALING_QUADRATIC = 3;
	
	/* Only change NUMBER_OF_CLASSES considering the
	 number of font size classes in templates/tagcloud.css
	Definition: We have 9 rank classes: 0,1,2,3,4,5,6,7,8 */
	private static final double NUMBER_OF_CLASSES = 9;
	
	/**
	 * Convert a List<LabelSearchResult> of unsorted popular labels into a
	 * TreeSet sorted by label frequency in descending order.
	 * 
	 * @param labels
	 *            TreeSet of most popular labels.
	 * @param scaleType
	 *            Scaling type used to scale the tag sizes (SCALING_LINEAR|SCALING_LOGARITHMIC|SCALING_QUADRATIC).
	 * @param sortType
	 *            Sorting type (SORTING_NAME|SORTING_RANK|SORTING_CIRCULAR).
	 * @return LinkedList<RankedLabelSearchResult>
	 */
	protected static LinkedList<RankedLabelSearchResult> getRankedLabels(TreeSet<RankedLabelSearchResult> labels,
			int scaleType, int sortType, int maxResults) {
		
		if (!labels.isEmpty()) {
			if (maxResults == -1) {
				maxResults = Integer.MAX_VALUE;
			}
			
			//Determine max and min label count for first maxResult labels
			int[] maxMinDataValues = new int[2];
			maxMinDataValues[0] = labels.first().getCount();

			RankedLabelSearchResult rankedlsr = null;
			Iterator<RankedLabelSearchResult> labelIterator = labels.iterator();

			for (int maxLabels = 0; maxLabels < maxResults; maxLabels++) {
				//Check if number of found labels < maxResults
				if (labelIterator.hasNext()) {
					rankedlsr = labelIterator.next();
					maxMinDataValues[1] = rankedlsr.getCount();
				} else {
					break;
				}
			}

			//Extract first maxResults labels
			SortedSet<RankedLabelSearchResult> rankedLabels = labels.headSet(rankedlsr,true);

			// Add labels with final rank scaling (based on relative label frequencies in extraction) ordered by name
			TreeSet<RankedLabelSearchResult> rankedFilteredLabels;
			
			switch (sortType) {
			case SORTING_NAME:
				rankedFilteredLabels = new TreeSet<RankedLabelSearchResult>(
						new RankedNameComparator());
				break;
			case SORTING_RANK:
				rankedFilteredLabels = new TreeSet<RankedLabelSearchResult>(
						new RankedRankComparator());
				break;
			case SORTING_CIRCULAR:
				rankedFilteredLabels = new TreeSet<RankedLabelSearchResult>(
						new RankedRankComparator());
				break;
			case SORTING_SIMILARITY:
				rankedFilteredLabels = new TreeSet<RankedLabelSearchResult>(
						new RankedNameComparator());
				break;
			default:
				rankedFilteredLabels = new TreeSet<RankedLabelSearchResult>(
						new RankedNameComparator());
			}
			for (RankedLabelSearchResult rlsr : rankedLabels) {
				rankedFilteredLabels.add(new RankedLabelSearchResult(rlsr.getLabel(), getRankScaling(
						rlsr.getCount(), maxMinDataValues, scaleType), rlsr.getCount()));
			}
			LinkedList<RankedLabelSearchResult> rankedFilteredLabelsList;
			Iterator<RankedLabelSearchResult> rankedLabelIterator;
			
			switch (sortType){
			case SORTING_CIRCULAR:
				/*
				 * Create a list of ranked labels in descending order
				 * and iteratively extract a label, alternating its add 
				 * procedure in the new list (left|right).
				 */
				rankedFilteredLabelsList = new LinkedList<RankedLabelSearchResult>();
				boolean left = true;
				int index = 0;
				rankedLabelIterator = rankedFilteredLabels.descendingIterator();
				while(rankedLabelIterator.hasNext()){
					RankedLabelSearchResult rlsr = rankedLabelIterator.next();
					if(left){
						rankedFilteredLabelsList.add(index,rlsr);
						left = false;
					} else {
						rankedFilteredLabelsList.add(++index,rlsr);
						left = true;
					}
					index = rankedFilteredLabelsList.indexOf(rlsr);
				}
				break;
			case SORTING_SIMILARITY:
				//TODO
				/*
				 * IDEA: Sort tags by similarity (based on co-occurrence)
				 * *
				 */
				rankedFilteredLabelsList = new LinkedList<RankedLabelSearchResult>();
				break;
			default:
				rankedFilteredLabelsList = new LinkedList<RankedLabelSearchResult>(rankedFilteredLabels);
			}

			return rankedFilteredLabelsList;
		} else{
			//No labels found
			return null;
		}
	}

	/**
	 * Scales the size of the labels based on max and min article per label
	 * values. Each label is assigned to one of the given classes, specifying
	 * the rank of this label in relation to other labels of the tagcloud.
	 * Default scaling is logarithmic, however quadratic and linear scaling are
	 * available as well.
	 * 
	 * @param docCount
	 *            Contains the number of articles per label.
	 * @param maxMinDataValues
	 *            Maximum and minimum number of articles per label.
	 * @param scale
	 *            Scaling type (SCALING_LINEAR|SCALING_LOGARITHMIC|SCALING_QUADRATIC).
	 * @return int
	 */
	protected static int getRankScaling(int docCount, int[] maxMinDataValues, int scale) {

		int maxDataValue = maxMinDataValues[0];
		int minDataValue = maxMinDataValues[1];

		double denominator;
		double internalRank = 0;

		//Calculate normalized internal rank ranging from 0..1
		switch (scale) {
		case SCALING_LINEAR:
			denominator = maxDataValue - minDataValue;
			if (denominator != 0)
				internalRank = (docCount - minDataValue) / denominator;
			break;
		case SCALING_LOGARITHMIC:
			denominator = Math.log(maxDataValue) - Math.log(minDataValue);
			if (denominator != 0)
				internalRank = (Math.log(docCount) - Math.log(minDataValue)) / denominator;
			break;
		case SCALING_QUADRATIC:
			denominator = Math.sqrt(maxDataValue) - Math.sqrt(minDataValue);
			if (denominator != 0)
				internalRank = (Math.sqrt(docCount) - Math.sqrt(minDataValue)) / denominator;
			break;
		}
		
		//Calculate rank class (integer) used by Velocity template/CSS file
		int calcRank = (int) Math.round(internalRank*(NUMBER_OF_CLASSES-1));
		//log.info("DocCount: "+docCount+" InternalRank: "+internalRank+" CalcRank: "+calcRank);
		
		return calcRank;

	}
}
