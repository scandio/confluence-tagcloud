/**
 * Copyright (c) 2011 Siemens AG - Corporate Technology, Bernd Lindner
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.siemens.confluence.tagcloud;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import com.atlassian.confluence.search.v2.SearchQuery;

/**
 * Query for the original creator of content.
 */
public class AllModifiersQuery implements SearchQuery
{
	private static final String KEY = "allModifiers";

	private final String modifier;
	
	public AllModifiersQuery(String modifier)
	{
		this.modifier = modifier;
	}

	public String getKey()
	{
		return KEY;
	}
	
	public String getModifier()
	{
		return modifier;
	}

	public List getParameters()
	{
		return Collections.singletonList(getModifier());
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;

		if (obj == this)
			return true;

		if (obj.getClass() != getClass())
			return false;
	
		AllModifiersQuery other = (AllModifiersQuery)obj;
		
		return new EqualsBuilder().append(modifier, other.modifier).isEquals();
	}

	@Override
	public int hashCode()
	{
		return new HashCodeBuilder(119,37).append(modifier).toHashCode();
	}	
}
