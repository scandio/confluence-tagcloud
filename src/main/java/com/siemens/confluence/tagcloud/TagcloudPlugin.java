/**
 * Copyright (c) 2016 Siemens AG - Corporate Technology, Bernd Lindner
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.siemens.confluence.tagcloud;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeSet;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.ConversionContextOutputType;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.persistence.dao.RankedLabelSearchResult;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.opensymphony.util.TextUtils;
import com.opensymphony.webwork.ServletActionContext;

public class TagcloudPlugin extends BaseMacro implements Macro {
    private static final String TAGCLOUD_MACRO_OCCURRENCE = "com.siemens.confluence.tagcloud.MacroOccurrance";
    private static final String FORMAT_CLOUD = "cloud";
    private static final String FORMAT_CLOUDSTATIC = "cloudStatic";
    private static final String FORMAT_CUMULUSFLASH = "cumulusFlash";
    private static final String FORMAT_CUMULUSCANVAS = "cumulusCanvas";
    private static final String FORMAT_WORDCLOUD = "wordcloud";
    private static final int DEFAULT_TSPEED = 100;
    private static final String DEFAULT_HICOLOR = "0x990000";
    private static final String DEFAULT_TCOLOR2 = "0x336699";
    private static final String DEFAULT_TCOLOR = "0x001E3D";
    private static final int DEFAULT_SIZE = 400;
    private static final String DEFAULT_TEMPLATE = "templates/tagcloudAsync.vm";
    private static final String CLOUDSTATIC_TEMPLATE = "templates/tagcloud.vm";
    private static final String CUMULUS_FLASH_TEMPLATE = "templates/cumulusFlash.vm";
    private static final String CUMULUS_CANVAS_TEMPLATE = "templates/cumulusCanvas.vm";
    private static final String ERROR_TEMPLATE = "templates/error.vm";
    private static final int DEFAULT_MAXRESULTS = 30;
    private static final int DEFAULT_DAYSBACK = -1;
    private static final int DEFAULT_TRUNCATE = -1;
    private static final String DEFAULT_SPACE = "@self";
    private static final int SCALING_LINEAR = 1;
    private static final int SCALING_LOGARITHMIC = 2;
    private static final int SCALING_QUADRATIC = 3;
    private static final int SORTING_NAME = 1;
    private static final int SORTING_RANK = 2;
    private static final int SORTING_CIRCULAR = 3;
    private static final int SORTING_SIMILARITY = 4;
    private static final int DEFAULT_FONTMINSIZE = 9;
    private static final int DEFAULT_FONTMAXSIZE = 25;
    private static final int DEFAULT_SCALING = SCALING_LOGARITHMIC;
    private static final int DEFAULT_SORTING = SORTING_NAME;
    private static final float DEFAULT_WORDCLOUDROTATERATIO = 0.5f;
    private static final String DEFAULT_SHAPE = "circle";
    private static final String[] SHAPE_VALUES = new String[] { "circle", "cardioid", "diamond", "pentagon", "star",
            "triangle-forward", "triangle-upright" };
    private static final String STR_DELIMITER = ",";
    private static final String STR_DELIMITER_REGEX = " *, *"; // ignore blanks

    /*
     * Only change NUMBER_OF_CLASSES considering the number of font size classes in templates/tagcloud.css Definition:
     * We have 9 rank classes: 0,1,2,3,4,5,6,7,8
     */
    private static final double NUMBER_OF_CLASSES = 9;
    private SearchManager searchManager;
    private LabelManager labelManager;
    private UserAccessor userAccessor;

    public boolean isInline() {
        return false;
    }

    public boolean hasBody() {
        return false;
    }

    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

    /*
     * XHTML implementation (Confluence 4)
     */
    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext)
            throws MacroExecutionException {
        try {
            // Is this the right way to get the PageContext (needed to determine the spaceKey)?
            DefaultConversionContext defaultConversionContext = (DefaultConversionContext) conversionContext;
            return execute(parameters, body, defaultConversionContext.getRenderContext());
        } catch (MacroException e) {
            throw new MacroExecutionException(e);
        }
    }

    public String execute(@SuppressWarnings("rawtypes") Map parameters, String body, RenderContext renderContext)
            throws MacroException {
        int macroOccurrence = setMacroOccurrence();
        Map<String, Object> contextMap = MacroUtils.defaultVelocityContext();
        @SuppressWarnings("unchecked")
        Map<String, Object> paramMap = getInputParameters(parameters, contextMap, renderContext);

        String velocityTemplate = (String) paramMap.get("template");
        @SuppressWarnings("unchecked")
        LinkedList<String> invalidParams = (LinkedList<String>) paramMap.get("invalidParams");

        if (invalidParams.isEmpty()) {
            if (velocityTemplate.equals(DEFAULT_TEMPLATE)) {
                // Only pass parameters to JS, findLabels is called in TagcloudJson.action
                contextMap.put("macroOccurrence", macroOccurrence);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                if (paramMap.get("startDateFormat") != null) {
                    contextMap.put("paramStartDate", sdf.format(paramMap.get("startDateFormat")));
                }
                if (paramMap.get("endDateFormat") != null) {
                    contextMap.put("paramEndDate", sdf.format(paramMap.get("endDateFormat")));
                }
                contextMap.put("paramSpace", (String) paramMap.get("space"));
                contextMap.put("paramTypes", (String) paramMap.get("types"));
                contextMap.put("paramUser", (String) paramMap.get("user"));
                contextMap.put("paramCotag", (String) paramMap.get("cotag"));
                contextMap.put("paramCotagIds", (String) paramMap.get("cotagIds"));
                contextMap.put("paramDisplayCotag", (Boolean) paramMap.get("displayCotag"));
                contextMap.put("paramExcludePrefix", (String) paramMap.get("excludePrefix"));
                contextMap.put("paramExcludePostfix", (String) paramMap.get("excludePostfix"));
                contextMap.put("paramMaxResults", (Integer) paramMap.get("maxResults"));
                contextMap.put("paramScale", (Integer) paramMap.get("scale"));
                contextMap.put("paramSort", (Integer) paramMap.get("sort"));
            } else {
                TreeSet<RankedLabelSearchResult> labels;
                labels = FindLabels.findLabels((Date) paramMap.get("startDateFormat"),
                        (Date) paramMap.get("endDateFormat"), (String) paramMap.get("space"),
                        (String) paramMap.get("types"), (String) paramMap.get("user"), (String) paramMap.get("cotag"),
                        (Boolean) paramMap.get("displayCotag"), (String) paramMap.get("excludePrefix"),
                        (String) paramMap.get("excludePostfix"), searchManager, labelManager,
                        (Integer) paramMap.get("maxResults"));
                LinkedList<RankedLabelSearchResult> labelsRanked = RankLabels.getRankedLabels(labels,
                        (Integer) paramMap.get("scale"), (Integer) paramMap.get("sort"),
                        (Integer) paramMap.get("maxResults"));
                contextMap.put("labels", labelsRanked);
            }
        } else {
            velocityTemplate = ERROR_TEMPLATE;
            contextMap.put("invalidParams", invalidParams);
        }
        return VelocityUtils.getRenderedTemplate(velocityTemplate, contextMap);
    }

    /**
     * Read a Map of input parameters and validate them. Then process them to the needed format/type and return them in
     * a Map.
     * 
     * @param parameters
     *            Contains the input parameters from the macro.
     * @param renderContext
     *            Contains the RenderContext from the macro.
     * @return Map<String,Object>
     */
    protected Map<String, Object> getInputParameters(Map<String, Object> parameters, Map<String, Object> contextMap,
            RenderContext renderContext) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        String velocityTemplate = DEFAULT_TEMPLATE;
        LinkedList<String> invalidParams = new LinkedList<String>();
        String outputType = renderContext.getOutputType();
        String format = ((String) parameters.get("format"));
        if (format == null) {
            format = "default";
        }
        if (ConversionContextOutputType.PDF.value().equals(outputType)
                || ConversionContextOutputType.WORD.value().equals(outputType)) {
            format = FORMAT_CLOUDSTATIC;
        }

        contextMap.put("format", format);

        int height = format.equals("default") || format.equals(FORMAT_CLOUDSTATIC) ? -1 : DEFAULT_SIZE;
        if (parameters.get("height") != null) {
            try {
                height = Integer.parseInt((String) parameters.get("height"));
            } catch (NumberFormatException e) {
                invalidParams.add("Invalid height number: '" + parameters.get("height") + "'");
            }
        }
        contextMap.put("height", height);

        int width = format.equals("default") || format.equals(FORMAT_CLOUDSTATIC) || format.equals(FORMAT_WORDCLOUD) ? -1
                : DEFAULT_SIZE;
        if (parameters.get("width") != null) {
            try {
                width = Integer.parseInt((String) parameters.get("width"));
            } catch (NumberFormatException e) {
                invalidParams.add("Invalid width number: '" + parameters.get("width") + "'");
            }
        }
        contextMap.put("width", width);

        int fontMinSize = DEFAULT_FONTMINSIZE;
        int fontMaxSize = DEFAULT_FONTMAXSIZE;
        if (parameters.get("fontMinSize") != null) {
            try {
                fontMinSize = Integer.parseInt((String) parameters.get("fontMinSize"));
            } catch (NumberFormatException e) {
                invalidParams.add("Invalid fontMinSize number: '" + parameters.get("fontMinSize") + "'");
            }
        }

        if (parameters.get("fontMaxSize") != null) {
            try {
                fontMaxSize = Integer.parseInt((String) parameters.get("fontMaxSize"));
            } catch (NumberFormatException e) {
                invalidParams.add("Invalid fontMaxSize number: '" + parameters.get("fontMaxSize") + "'");
            }
        }
        if (format.equals(FORMAT_CUMULUSFLASH)) {
            // Normalize fonts in the flash movie, as they are scaled depending on width/height.
            // They look normal for width~100px.
            float normalization = 100f / Math.min(height, width);
            fontMinSize = Math.round(fontMinSize * normalization);
            fontMaxSize = Math.round(fontMaxSize * normalization);
        }
        contextMap.put("fontMinSize", fontMinSize);

        // Used for calculating the actual font size by multiplying with rank in Velocity or JS.
        // So we get an equal distribution of discrete font sizes in the given range. Float with 1
        // digit after the decimal point.
        contextMap.put("fontSizeStep",
                (float) Math.round((fontMaxSize - fontMinSize) / (NUMBER_OF_CLASSES - 1) * 10) / 10);

        if (format.equals(FORMAT_CUMULUSFLASH) || format.equals(FORMAT_CUMULUSCANVAS)) {
            velocityTemplate = format.equals(FORMAT_CUMULUSFLASH) ? CUMULUS_FLASH_TEMPLATE : CUMULUS_CANVAS_TEMPLATE;
            String cumulusTColor = DEFAULT_TCOLOR;
            String cumulusTColor2 = DEFAULT_TCOLOR2;
            String cumulusHiColor = DEFAULT_HICOLOR;
            int cumulusTSpeed = DEFAULT_TSPEED;

            if (parameters.get("cumulusTColor") != null) {
                if (validateColor((String) parameters.get("cumulusTColor"))) {
                    cumulusTColor = (String) parameters.get("cumulusTColor");
                } else {
                    invalidParams.add("Invalid cumulusTColor: '" + parameters.get("cumulusTColor") + "'");
                }
            }
            if (parameters.get("cumulusTColor2") != null) {
                if (validateColor((String) parameters.get("cumulusTColor2"))) {
                    cumulusTColor2 = (String) parameters.get("cumulusTColor2");
                } else {
                    invalidParams.add("Invalid cumulusTColor2: '" + parameters.get("cumulusTColor2") + "'");
                }
            }
            if (parameters.get("cumulusHiColor") != null) {
                if (validateColor((String) parameters.get("cumulusHiColor"))) {
                    cumulusHiColor = (String) parameters.get("cumulusHiColor");
                } else {
                    invalidParams.add("Invalid cumulusHiColor: '" + parameters.get("cumulusHiColor") + "'");
                }
            }
            if (parameters.get("cumulusTSpeed") != null) {
                try {
                    cumulusTSpeed = Integer.parseInt((String) parameters.get("cumulusTSpeed"));
                } catch (NumberFormatException e) {
                    invalidParams.add("Invalid cumulusTSpeed number: '" + parameters.get("cumulusTSpeed") + "'");
                }
            }
            contextMap.put("tcolor",
                    format.equals(FORMAT_CUMULUSFLASH) ? cumulusTColor : cumulusTColor.replaceFirst("0x", "#"));
            contextMap.put("tcolor2",
                    format.equals(FORMAT_CUMULUSFLASH) ? cumulusTColor2 : cumulusTColor2.replaceFirst("0x", "#"));
            contextMap.put("hicolor",
                    format.equals(FORMAT_CUMULUSFLASH) ? cumulusHiColor : cumulusHiColor.replaceFirst("0x", "#"));
            contextMap
                    .put("tspeed", format.equals(FORMAT_CUMULUSFLASH) ? cumulusTSpeed : (double) cumulusTSpeed / 2000);
        } else if (format.equals(FORMAT_WORDCLOUD)) {
            float wordcloudRotateRatio = DEFAULT_WORDCLOUDROTATERATIO;
            if (parameters.get("wordcloudRotateRatio") != null) {
                try {
                    wordcloudRotateRatio = Float.parseFloat((String) parameters.get("wordcloudRotateRatio"));
                } catch (NumberFormatException e) {
                    invalidParams.add("Invalid wordcloudRotateRatio number: '" + parameters.get("wordcloudRotateRatio")
                            + "'");
                }
                if (wordcloudRotateRatio < 0 || wordcloudRotateRatio > 1) {
                    invalidParams.add("Invalid wordcloudRotateRatio number: '" + parameters.get("wordcloudRotateRatio")
                            + "'. Expected a value between 0 and 1.");
                }
            }
            contextMap.put("wordcloudRotateRatio", wordcloudRotateRatio);

            String shape = DEFAULT_SHAPE;
            String shapeParam = (String) parameters.get("shape");
            if (shapeParam != null) {
                if (Arrays.asList(SHAPE_VALUES).contains(shapeParam)) {
                    shape = shapeParam;
                } else {
                    invalidParams.add("Invalid shape type: '" + shapeParam + "'");
                }
            }
            contextMap.put("shape", shape);

            velocityTemplate = DEFAULT_TEMPLATE;
        } else if (format.equals(FORMAT_CLOUDSTATIC)) {
            velocityTemplate = CLOUDSTATIC_TEMPLATE;
        } else if (format.equals("default")) {
            // classical tagcloud
        } else {
            invalidParams.add("Invalid format type: '" + format + "'");
        }

        int scaleType = DEFAULT_SCALING;
        String scale = (String) parameters.get("scale");
        if (TextUtils.stringSet(scale)) {
            if (scale.matches("linear"))
                scaleType = SCALING_LINEAR;
            else if (scale.matches("logarithmic"))
                scaleType = SCALING_LOGARITHMIC;
            else if (scale.matches("quadratic"))
                scaleType = SCALING_QUADRATIC;
            else
                invalidParams.add("Invalid scaling type: '" + scale + "'");
        }
        paramMap.put("scale", scaleType);

        int sortType = DEFAULT_SORTING;
        String sort = ((String) parameters.get("sort"));
        if (TextUtils.stringSet(sort)) {
            if (sort.matches("name"))
                sortType = SORTING_NAME;
            else if (sort.matches("rank"))
                sortType = SORTING_RANK;
            else if (sort.matches("circular"))
                sortType = SORTING_CIRCULAR;
            else if (sort.matches("similarity"))
                sortType = SORTING_SIMILARITY;
            else
                invalidParams.add("Invalid sorting type: '" + sort + "'");
        }
        paramMap.put("sort", sortType);

        int maxResults = DEFAULT_MAXRESULTS;
        if (parameters.get("maxResults") != null) {
            try {
                maxResults = Integer.parseInt((String) parameters.get("maxResults"));
            } catch (NumberFormatException e) {
                invalidParams.add("Invalid maximum number of tags: '" + parameters.get("maxResults") + "'");
            }
        } else if (parameters.get("max") != null) {
            try {
                maxResults = Integer.parseInt((String) parameters.get("max"));
            } catch (NumberFormatException e) {
                invalidParams.add("Invalid maximum number of tags: '" + parameters.get("max") + "'");
            }
        }
        paramMap.put("maxResults", maxResults);

        int daysBack = DEFAULT_DAYSBACK;
        Date startDateFormat = null;
        Date endDateFormat = null;
        if (parameters.get("daysBack") != null) {
            try {
                daysBack = Integer.parseInt((String) parameters.get("daysBack"));
            } catch (NumberFormatException e) {
                invalidParams.add("Invalid number of daysBack: '" + parameters.get("daysBack") + "'");
            }
            if (daysBack != -1) {
                Calendar cal = Calendar.getInstance();
                endDateFormat = cal.getTime();
                // current day is included, so we need to shift by 1 day to get an accurate period
                cal.add(Calendar.DATE, -daysBack+1);
                startDateFormat = cal.getTime();
            }
        } else if (parameters.get("startDate") != null || parameters.get("endDate") != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setLenient(false);
            try {
                if (parameters.get("startDate") != null) {
                    startDateFormat = sdf.parse((String) parameters.get("startDate"));
                }
                if (parameters.get("endDate") != null) {
                    endDateFormat = sdf.parse((String) parameters.get("endDate"));
                }
            } catch (ParseException e) {
                invalidParams.add("Invalid date format: '" + parameters.get("startDate") + "' '"
                        + parameters.get("endDate") + "'");
            }
            // only startDate is set - use range until today
            if (endDateFormat == null) {
                endDateFormat = Calendar.getInstance().getTime();
            }
            // only endDate is set - use range since 1990
            if (startDateFormat == null) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, 1990);
                startDateFormat = cal.getTime();
            }
        }
        paramMap.put("startDateFormat", startDateFormat);
        paramMap.put("endDateFormat", endDateFormat);

        int truncateSize = DEFAULT_TRUNCATE;
        if (parameters.get("truncate") != null) {
            try {
                truncateSize = Integer.parseInt((String) parameters.get("truncate"));
            } catch (NumberFormatException e) {
                invalidParams.add("Invalid truncate characters number: '" + parameters.get("truncate") + "'");
            }
        }
        contextMap.put("truncateSize", truncateSize);

        String excludePrefix = null;
        if (parameters.get("excludePrefix") != null) {
            excludePrefix = (String) parameters.get("excludePrefix");
        }
        paramMap.put("excludePrefix", excludePrefix);

        String excludePostfix = null;
        if (parameters.get("excludePostfix") != null) {
            excludePostfix = (String) parameters.get("excludePostfix");
        }
        paramMap.put("excludePostfix", excludePostfix);

        String spaceKey = DEFAULT_SPACE;
        if (parameters.get("space") != null) {
            spaceKey = ((String) parameters.get("space")).trim();
        } else if (parameters.get("spaces") != null) {
            spaceKey = ((String) parameters.get("spaces")).trim();
        }
        if (spaceKey != null) {
            if (spaceKey.equals("@all")) {
                spaceKey = null;
            } else if (spaceKey.equals("@self")) {
                PageContext pageContext = (PageContext) renderContext;
                spaceKey = pageContext.getSpaceKey();
            }
            // used to build URL (label list for ONE space)
            if (spaceKey != null && spaceKey.indexOf(STR_DELIMITER) == -1) {
                contextMap.put("singleTargetSpace", spaceKey);
            }
        }
        paramMap.put("space", spaceKey);

        String types = null;
        if (parameters.get("types") != null) {
            types = ((String) parameters.get("types")).trim().toLowerCase();
            if (!(types.contains(Page.CONTENT_TYPE) || types.contains(BlogPost.CONTENT_TYPE))) {
                invalidParams.add("Invalid Content Types: '" + parameters.get("types") + "'");
            }
        }
        paramMap.put("types", types);

        String cotag = (String) parameters.get("cotag");
        // Construct path to access label view for related labels using Confluence's
        // viewlabel.action
        if (TextUtils.stringSet(cotag)) {
            cotag = cotag.trim();
            String[] cotags = cotag.split(STR_DELIMITER_REGEX);
            String cotagListUrl = "/labels/viewlabel.action?";
            String cotagIds = "";
            for (int i = 0; i < cotags.length; i++) {
                Label coLabel = labelManager.getLabel(cotags[i]);
                if (coLabel != null) {
                    cotagListUrl += "&ids=" + coLabel.getId();
                    if (!cotagIds.isEmpty()) {
                        cotagIds += STR_DELIMITER;
                    }
                    cotagIds += coLabel.getId();
                }
            }
            // Html suffix to avoid escaping "&", see
            // https://developer.atlassian.com/display/CONFDEV/Enabling+XSS+Protection+in+Plugins
            contextMap.put("cotagListUrlHtml", cotagListUrl);
            paramMap.put("cotag", cotag);
            paramMap.put("cotagIds", cotagIds);
        }
        boolean displayCotag = true;
        if (parameters.get("displayCotag") != null) {
            displayCotag = Boolean.parseBoolean(parameters.get("displayCotag").toString());
        }
        paramMap.put("displayCotag", displayCotag);

        // since Confluence 5.2 lucene stores user key instead of user name
        String userKey = null;
        if (parameters.get("user") != null) {
            ConfluenceUser user = userAccessor.getUserByName((String) parameters.get("user"));
            if (user != null) {
                userKey = user.getKey().getStringValue();
            }
        }
        paramMap.put("user", userKey);

        paramMap.put("invalidParams", invalidParams);
        paramMap.put("template", velocityTemplate);
        paramMap.put("contextMap", contextMap);

        return paramMap;
    }

    public void setSearchManager(SearchManager manager) {
        this.searchManager = manager;
    }

    public void setLabelManager(LabelManager manager) {
        this.labelManager = manager;
    }

    public void setUserAccessor(UserAccessor userAccessor) {
        this.userAccessor = userAccessor;
    }

    /**
     * Validate the color format 0xRRGGBB
     * 
     * @param color
     *            Input-String from the user
     * @return true, if the color correct
     */
    private boolean validateColor(String color) {
        String colorPattern = "0x[0-9a-fA-F]{6}";
        Pattern p = Pattern.compile(colorPattern, Pattern.CASE_INSENSITIVE | Pattern.DOTALL | Pattern.MULTILINE);
        return p.matcher(color).matches();
    }

    /**
     * Must be called exactly once for each macro execution to generate a page-unique id needed by JavaScript.
     * 
     * @return Current occurrence / call of this macro on the same page / request
     */
    private int setMacroOccurrence() {
        int macroOccurrance = 0;
        HttpServletRequest request = ServletActionContext.getRequest();
        // request is null in Macro Browser. No problem, as there is only one Macro rendered.
        if (request != null) {
            if (request.getAttribute(TAGCLOUD_MACRO_OCCURRENCE) != null) {
                macroOccurrance = (Integer) request.getAttribute(TAGCLOUD_MACRO_OCCURRENCE);
            }
            macroOccurrance++;
            request.setAttribute(TAGCLOUD_MACRO_OCCURRENCE, macroOccurrance);
        }
        return macroOccurrance;
    }
}
