/**
 * Copyright (c) 2016 Siemens AG - Corporate Technology, Bernd Lindner
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.siemens.confluence.tagcloud;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeSet;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.labels.persistence.dao.RankedLabelSearchResult;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.util.GeneralUtil;
import com.opensymphony.util.TextUtils;

/**
 * Note: Passed parameters are not validated or defaulted in this action. This is done in the Tagcloud Macro and passed
 * via AJS.Meta.
 * 
 */
public class TagcloudJson extends ConfluenceActionSupport implements Beanable {

    private static final long serialVersionUID = 8396110749202304178L;
    private SearchManager searchManager;

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private Date startDateFormat;
    private Date endDateFormat;
    private String space;
    private String types;
    private String user;
    private String cotag;
    private boolean displayCotag = true;
    private String excludePrefix;
    private String excludePostfix;
    private int maxResults;
    private int scale;
    private int sort;

    LinkedList<RankedLabelSearchResult> labelsRanked = null;

    public String execute() throws Exception {
        TreeSet<RankedLabelSearchResult> labels;
        labels = FindLabels.findLabels(startDateFormat, endDateFormat, space, types, user, cotag, displayCotag,
                excludePrefix, excludePostfix, searchManager, labelManager, maxResults);
        labelsRanked = RankLabels.getRankedLabels(labels, scale, sort, maxResults);

        return SUCCESS;
    }

    @Override
    public Object getBean() {
        if (labelsRanked == null) {
            return null;
        }

        Map<String, Object> result = new HashMap<String, Object>();

        ArrayList<Map<String, Object>> labels = new ArrayList<Map<String, Object>>();
        for (RankedLabelSearchResult rankedLabelSearchResult : labelsRanked) {
            Map<String, Object> labelData = new HashMap<String, Object>();
            labelData.put("title", rankedLabelSearchResult.getLabel().getDisplayTitle());
            labelData.put("id", rankedLabelSearchResult.getLabel().getId());
            labelData.put("count", rankedLabelSearchResult.getCount());
            labelData.put("rank", rankedLabelSearchResult.getRank());
            labelData.put("isSimpleUrlPath", isSimpleUrlPath(rankedLabelSearchResult.getLabel().getDisplayTitle()));
            labels.add(labelData);
        }
        result.put("labels", labels);

        return result;
    }

    private boolean isSimpleUrlPath(String title) {
        return !TextUtils.stringSet(cotag) && GeneralUtil.isSafeTitleForUrl(title);
    }

    public void setStartDate(String startDate) {
        if (startDate != null) {
            sdf.setLenient(false);
            try {
                startDateFormat = sdf.parse(startDate);
            } catch (ParseException e) {
                // Ignore. We expect validation from Tagcloud Macro.
            }
        }
    }

    public void setEndDate(String endDate) {
        if (endDate != null) {
            sdf.setLenient(false);
            try {
                endDateFormat = sdf.parse(endDate);
            } catch (ParseException e) {
                // Ignore. We expect validation from Tagcloud Macro.
            }
        }
    }

    public void setSpace(String space) {
        this.space = space;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setCotag(String cotag) {
        this.cotag = cotag;
    }

    public void setDisplayCotag(String displayCotag) {
        if (displayCotag != null) {
            this.displayCotag = Boolean.parseBoolean(displayCotag);
        }
    }

    public void setExcludePrefix(String excludePrefix) {
        this.excludePrefix = excludePrefix;
    }

    public void setExcludePostfix(String excludePostfix) {
        this.excludePostfix = excludePostfix;
    }

    public void setMaxResults(String maxResults) {
        if (maxResults != null) {
            this.maxResults = Integer.parseInt(maxResults);
        }
    }

    public void setScale(String scale) {
        if (scale != null) {
            this.scale = Integer.parseInt(scale);
        }
    }

    public void setSort(String sort) {
        if (sort != null) {
            this.sort = Integer.parseInt(sort);
        }
    }

    public void setSearchManager(SearchManager manager) {
        this.searchManager = manager;
    }

}
