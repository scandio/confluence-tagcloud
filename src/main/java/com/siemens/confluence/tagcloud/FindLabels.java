/**
 * Copyright (c) 2016 Siemens AG - Corporate Technology, Bernd Lindner
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.siemens.confluence.tagcloud;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;

import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.actions.RankedRankComparator;
import com.atlassian.confluence.labels.persistence.dao.RankedLabelSearchResult;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.v2.BooleanOperator;
import com.atlassian.confluence.search.v2.ContentSearch;
import com.atlassian.confluence.search.v2.ISearch;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchConstants;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.search.v2.SearchResults;
import com.atlassian.confluence.search.v2.SearchSort;
import com.atlassian.confluence.search.v2.lucene.LuceneSearchResults;
import com.atlassian.confluence.search.v2.query.BooleanQuery;
import com.atlassian.confluence.search.v2.query.ContentTypeQuery;
import com.atlassian.confluence.search.v2.query.DateRangeQuery;
import com.atlassian.confluence.search.v2.query.DateRangeQuery.DateRangeQueryType;
import com.atlassian.confluence.search.v2.query.InSpaceQuery;
import com.atlassian.confluence.search.v2.query.LabelQuery;
import com.atlassian.confluence.search.v2.query.TextFieldQuery;
import com.atlassian.confluence.search.v2.searchfilter.SiteSearchPermissionsSearchFilter;
import com.atlassian.confluence.search.v2.sort.ModifiedSort;
import com.google.common.collect.Lists;

public class FindLabels {
	private static final String STR_DELIMITER_REGEX = " *, *"; // ignore blanks
	private static final Logger log = Logger.getLogger(FindLabels.class);

	/**
	 * Performs a search for types PAGE/BLOG matching the given parameters in a Lucene index. Based
	 * on the result set, labels (tags) are extracted, counted and ranked by count.
	 * 
	 * @param startDate
	 *            Start date for time period that should be regarded.
	 * @param endDate
	 *            End date for time period that should be regarded.
	 * @param spaceKey
	 *            Spaces that are used for filtering the cloud.
	 * @param user
	 *            Limit result set to pages edited by a certain user.
	 * @param cotag
	 *            Limit result set to pages that include a certain tag.
	 * @param displayCotag
	 *            Indicate whether the cotag is shown in the cloud.
	 * @param excldPre
	 *            Remove tags starting with a given prefix
	 * @param excldPost
	 *            Remove tags starting with a given postfix.
	 * @return TreeSet<RankedLabelSearchResult>
	 */
	protected static TreeSet<RankedLabelSearchResult> findLabels(Date startDate, Date endDate,
			String spaceKey, String types, String user, String cotag, boolean displayCotag, String excldPre,
			String excldPost, SearchManager searchManager, LabelManager labelManager, int maxResults) {

		Set<SearchQuery> composedSubQuery = new HashSet<SearchQuery>();
		if (user != null) {
			composedSubQuery.add(new AllModifiersQuery(user));
		}
		if (spaceKey != null) {
			String[] spaces = spaceKey.split(STR_DELIMITER_REGEX);
			Set<SearchQuery> composedBooleanSpaceQuery = new HashSet<SearchQuery>();
			for (String space : spaces) {
				composedBooleanSpaceQuery.add(new InSpaceQuery(space));
			}
			composedSubQuery.add(BooleanQuery.composeOrQuery(composedBooleanSpaceQuery));
		}
		if (startDate != null && endDate != null) {
		    // include endDate's day
		    Calendar cal = Calendar.getInstance();
            cal.setTime(endDate);
            cal.add(Calendar.HOUR_OF_DAY, 23);
            cal.add(Calendar.MINUTE, 59);
            cal.add(Calendar.SECOND, 59);
            endDate = cal.getTime();
			composedSubQuery.add(new DateRangeQuery(startDate, endDate, true, true,
					DateRangeQueryType.MODIFIED));
		}

		// Only search pages that contain labels/tags.
		// The following line can also be commented, but makes the Lucene query itself more
		// inefficient as all pages are searched instead of only those with tags.
		composedSubQuery.add(new TextFieldQuery("label", "global*", BooleanOperator.OR));

		String[] cotags = null;
		if (cotag != null) {
			cotags = cotag.split(STR_DELIMITER_REGEX);
			Set<SearchQuery> composedBooleanCotagQuery = new HashSet<SearchQuery>();
			for (String tag : cotags) {
				composedBooleanCotagQuery.add(new LabelQuery(tag));
			}
			composedSubQuery.add(BooleanQuery.composeAndQuery(composedBooleanCotagQuery));
		}

		// Only content types of PAGE and BLOG can have labels
        Collection<ContentTypeEnum> searchTypes = new LinkedList<ContentTypeEnum>();
		if (types != null) {
		    if (types.contains(Page.CONTENT_TYPE)) {
		        searchTypes.add(ContentTypeEnum.PAGE);
		    }
            if (types.contains(BlogPost.CONTENT_TYPE)) {
                searchTypes.add(ContentTypeEnum.BLOG);
            }
		} else {
		    // all
		    searchTypes.add(ContentTypeEnum.PAGE);
		    searchTypes.add(ContentTypeEnum.BLOG);
		}
		composedSubQuery.add(new ContentTypeQuery(searchTypes));

		SearchQuery query = BooleanQuery.composeAndQuery(composedSubQuery);

		// Sorting is not relevant in this case, but the Search object needs a defined sorting mode
		SearchSort sort = new ModifiedSort(SearchSort.Order.DESCENDING);

		ArrayList<SearchResult> allSearchResults = Lists.newArrayList();
		allSearchResults = doSearch(searchManager, query, sort, 0, allSearchResults);

		// Count label occurrences
		HashMap<String, Integer> labelMap = new HashMap<String, Integer>();
		Integer labelValue;
		for (SearchResult searchResult : allSearchResults) {
			Set<String> labelSet = searchResult.getLabels(null);
			if (!displayCotag && cotags != null) {
				for (String tag : cotags) {
					labelSet.remove(tag);
				}
			}
			for (String label : labelSet) {
				if (labelMap.containsKey(label)) {
					labelValue = (Integer) labelMap.get(label);
					labelValue++;
				} else {
					labelValue = 1;
				}
				labelMap.put(label, labelValue);
			}
		}

		// Arrange labels by number of occurrence (rank based on count)
		TreeSet<RankedLabelSearchResult> rlsrSet = new TreeSet<RankedLabelSearchResult>(
				new RankedRankComparator());

		String[] excldPreArray = null;
		;
		if (excldPre != null) {
			excldPreArray = excldPre.split(STR_DELIMITER_REGEX);
		}
		String[] excldPostArray = null;
		if (excldPost != null) {
			excldPostArray = excldPost.split(STR_DELIMITER_REGEX);
		}
		if (maxResults == -1) {
			maxResults = Integer.MAX_VALUE;
		}
		int counter = 0;
		// First sort the map by value to increase performance (bootle neck labelManager.getLabel)
		// and take only the labels until maxResults.
		// Performance comparison with 6352 tags and maxResult=200:
		// without sortByValue: ~40s
		// with sortByValue: ~1.5s
		for (Map.Entry<String, Integer> entry : sortByValue(labelMap).entrySet()) {
			if (!isTagOnBlacklist(entry.getKey(), excldPreArray, excldPostArray)) {
				// Initialize RankedLabelSearchResult(label,count,count) instead of
				// RankedLabelSearchResult(label,rank,count)
				Label l = labelManager.getLabel(entry.getKey());
				// Had situations where label contains commas, and getLabel returns null.
				// https://jira.atlassian.com/browse/CONF-32131 ?
				if (l != null) {
				    rlsrSet.add((new RankedLabelSearchResult(l, entry.getValue(), entry.getValue())));
				    counter++;
				}
				// Only until maxResults (increase performance)
				if (counter >= maxResults)
					break;
			}
		}
		return rlsrSet;
	}

	/**
	 * Recursive Lucene search (kind of "paging") to overcome 500 pages limitation since Confluence
	 * 5.
	 * 
	 * @see https://jira.atlassian.com/browse/CONF-32560
	 * @see https
	 *      ://answers.atlassian.com/questions/258816/is-the-lucene-query-search-limited-to-500-
	 *      results-in-confluence-5
	 * 
	 * @param searchManager
	 * @param query
	 * @param sort
	 * @param startOffset
	 *            0 for first call
	 * @param allSearchResults
	 * @return allResults
	 */
	private static ArrayList<SearchResult> doSearch(SearchManager searchManager, SearchQuery query,
			SearchSort sort, int startOffset, ArrayList<SearchResult> allSearchResults) {
		final ISearch search = new ContentSearch(query, sort,
				SiteSearchPermissionsSearchFilter.getInstance(), startOffset,
				SearchConstants.MAX_LIMIT);
		SearchResults searchResults;
		try {
			searchResults = searchManager.search(search);
		} catch (InvalidSearchException e) {
			log.error("InvalidSearchException: " + e);
			searchResults = LuceneSearchResults.EMPTY_RESULTS;
		}
		allSearchResults.addAll(searchResults.getAll());
		log.debug("Search results size: " + searchResults.size() + " of total "
				+ searchResults.getUnfilteredResultsCount());
		if (searchResults.getUnfilteredResultsCount() > (startOffset + SearchConstants.MAX_LIMIT)) {
			return doSearch(searchManager, query, sort, (startOffset + SearchConstants.MAX_LIMIT),
					allSearchResults);
		} else {
			return allSearchResults;
		}
	}

	/**
	 * Checks whether a tag should excluded based on excldPreArray an excldPostArray.
	 * 
	 * @param tag
	 *            String representation of the tag to be checked.
	 * @param excldPreArray
	 *            Array of prefixes to be excluded.
	 * @param excldPostArray
	 *            Array of postfixes to be excluded.
	 * @return boolean
	 */
	protected static boolean isTagOnBlacklist(String tag, String[] excldPreArray,
			String[] excldPostArray) {
		if (excldPreArray != null) {
			for (String excldPreStr : excldPreArray) {
				if (tag.startsWith(excldPreStr)) {
					return true;
				}
			}
		}
		if (excldPostArray != null) {
			for (String excldSufStr : excldPostArray) {
				if (tag.endsWith(excldSufStr)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Converts a Map into a Map with sorted values
	 * 
	 * @param map
	 *            Map, that should be sorted (Value extends Comparable)
	 * @return Sorted map
	 */
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
		List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
				return (o2.getValue()).compareTo(o1.getValue());
			}
		});
		Map<K, V> result = new LinkedHashMap<K, V>();
		for (Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}
}
